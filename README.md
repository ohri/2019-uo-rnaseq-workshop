# 2019 uO RNASeq Workshop

## Workshop Dates
June 4, 2019: 8:30AM - 5:00PM


## Workshop Location
Roger-Guindon Hall  
Room 4161  
University of Ottawa  
451 Smyth Road  
Ottawa, ON K1H 8L1  

## Organizers		
* Dr. Jonathan Lee, University of Ottawa 
* Gareth Palidwor, Chris Porter; Ottawa Bioinformatics Core Facility, Ottawa Hospital Research Institute

## Workshop Files

[Workshop Agenda](2019_uOttawa_RNA-Seq_analysis_workshop_Agenda.docx)

[Lecture 1: Sequencing Data Background and Review](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/2019_Workshop_Lecture_1.pptx)
* Example FASTQC files
    * [ERR975344.1_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.1_fastqc.html?inline=false)
    * [ERR975344.2_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.2_fastqc.html?inline=false)
* Example RNASEQC files
    * [Activated.1](http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Activated.1/)
    * [Quiescent.1](http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Quiescent.1/)
* Example BAM files for IGV Exercise
    * [in_day1_rep1_chr12.bam](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/in_day1_rep1_chr12.bam?inline=false)
    * [in_day1_rep1_chr12.bam.bai](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/in_day1_rep1_chr12.bam.bai?inline=false)
* [IGV instructions](Files/IGV.md)

[R package installation script](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/install_packages.R)

[Lecture 2: Differential Gene Expression with DESeq2](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/2019_Workshop_Lecture_2-DESeq2.pptx)

[R Script for RNASeq Data Analysis](Files/analysis_script.Rmd?inline=false)
* [Ranked significan gene list output g:Profiler exercise](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/Ensembl_Act_vs_Qui_sig05_salmon.txt?inline=false)
* [Full ranked gene list ouptput for GSEA exercise](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/Ensembl_Act_vs_Qui_all_salmon.txt?inline=false)

[Lecture 3: Gene Set Enrichment Analysis](https://gitlab.com/ohri/2019-uo-rnaseq-workshop/raw/master/Files/2019_Workshop_Lecture_3_-_Annotation_Enrichment.ppt)
* [g:Profiler instructions](Files/gprofiler.md)


