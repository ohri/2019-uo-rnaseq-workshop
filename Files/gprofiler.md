Go to the [g:Profiler site](https://biit.cs.ut.ee/gprofiler/gost)

Download:
* the [ordered list of up-regulated signficant genes](Files/Act_vs_Qui_up_ensembl.txt) from the Act vs Qui comparison
* the [ordered list of down-regulated signficant genes](Files/Act_vs_Qui_down_ensembl.txt) from the Act vs Qui comparison
* the [list of background genes](Files/Act_vs_Qui_all_ensembl.txt) from the Act vs Qui comparison 

Click on the _Upload query_, then click on the upload query box and select the `Act_vs_Qui_up_ensembl.txt` or `Act_vs_Qui_down_ensembl.txt` file

Under data sources select only:
* GO biological process
* KEGG

Click "Run query" and scroll to the bottom of the page.

Select the "Detailed Results" tab.

Click on the "Term size" checkbox; enter `3` (min) in the left hand box and `500` (max) in the right hand box.

Review the results; do these make sense for for terms that would sgnificantly enriched in genes differing between Activated and Quiescent muscle satellite cells?
Are any surprising?

Click on "CSV" and save the results.

Go back up to the query options and select the "Advanced Options", select "Custom" statistical domain scope. Open the `Ensembl_Act_vs_Qui_all_salmon.txt` file in a text editor, select all text, copy, then paste it into the text field below "background".

Click "Run query" again.

Click on the "Term size" checkbox; enter `3` (min) in the left hand box and `500` (max) in the right hand box.

How do these results differ from the prior results? Why do they differ?




