## uOttawa RNASeq Workshop 2019: IGV Tutorial

Download the following files to the same directory:
* [in_day1_rep1_chr12.bam](in_day1_rep1_chr12.bam)
* [in_day1_rep1_chr12.bam.bai](in_day1_rep1_chr12.bai)

Exercise:
* Open IGV
* On the top left of the IGV browser, select the human (hg38). We previously aligned to this
* load the above bam file (not the .bai which is the index) using the menu item: 
    * File | Load From File
* select the in_day1_rep1_chr12.bam file
* Select chr12 from the pull down next to the genome pull down
* Type TBK1 in the window to the right of the chromosome pull down
* Use the [+]/[-] zoom buttons on the top right to zoom in on the data

Questions:
* Why are there reads in the introns?
